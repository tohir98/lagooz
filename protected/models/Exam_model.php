<?php

/**
 * Description of Exam_model
 *
 * @author TOHIR
 */
class Exam_model {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scoreEntryEmployees($batch_id) {
        $sql = "SELECT DISTINCT es.employee_id, e.first_name, e.last_name, e.employee_number "
                . "FROM employees_subjects es "
                . "INNER JOIN subjects s ON es.subject_id = s.id "
                . "INNER JOIN employees e ON es.employee_id = e.id "
                . "WHERE batch_id={$batch_id} ";

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function scoreEntrySubjects($batch_id, $employee_id) {
        $sql = "SELECT es.employee_id, e.employee_number, s.`name`, es.subject_id "
                . "FROM employees_subjects es "
                . "INNER JOIN subjects s ON es.subject_id = s.id "
                . "INNER JOIN employees e ON es.employee_id = e.id "
                . "WHERE batch_id = {$batch_id}  AND es.employee_id = {$employee_id} ";

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function processStudentScore($params) {
        $studentScore = new StudentScore();
        $data = $params['score'];
        for ($i = 0; $i < count($data['id']); $i++) {
            $where = array(
                'stuid' => $data['id'][$i],
                'sub_id' => $params['subject_id'],
                'cid' => $params['batch_id'],
                'session_id' => $params['session_id'],
                'term_id' => $params['term_id'],
            );

            if (!$studentScore::getStudentScores($where)) {
                $data_db = array_merge($where, array(
                    'emp_id' => $params['employee_id'],
                    'ca' => $data['ca'][$i],
                    'exam' => $data['exam'][$i],
                    'date_created' => date('Y-m-d h:i:s')
                ));
                Yii::app()->db->createCommand()->insert('student_score', $data_db)->execute;
            } else {
                $updateSql = "UPDATE student_score SET ca = " . $data['ca'][$i] . ", exam = " . $data['exam'][$i] . " WHERE stuid = " . $data['id'][$i] . " AND sub_id = " . $params['subject_id'] . " AND cid = " . $params['batch_id'] . " AND session_id = " . $params['session_id'] . " AND term_id = " . $params['term_id'];
                Yii::app()->db->createCommand($updateSql)->execute();
            }
        }
        return true;
    }

    public function updateStudentScore($data) {
        //var_dump($data); exit;
        if ($this->fetch_student_score(['session_id' => $data['session_id'], 'stuid' => $data['student_id'], 'term_id' => $data['term_id'], 'cid' => $data['batch_id'], 'sub_id' => $data['subject_id']])) {
            $updateSql = "UPDATE student_score SET ca = " . $data['ca'] . ", exam = " . $data['exam'] . " WHERE stuid = " . $data['student_id'] . " AND sub_id = " . $data['subject_id'] . " AND cid = " . $data['batch_id'] . " AND session_id = " . $data['session_id'] . " AND term_id = " . $data['term_id'];
            Yii::app()->db->createCommand($updateSql)->execute();
        }else{
            $data_db = array(
                    'emp_id' => NULL, //Yii::app()->user->Id,
                    'session_id' => $data['session_id'],
                    'term_id' => $data['term_id'],
                    'sub_id' => $data['subject_id'],
                    'stuid' => $data['student_id'],
                    'cid' => $data['batch_id'],
                    'ca' => $data['ca'],
                    'exam' => $data['exam'],
                    'date_created' => date('Y-m-d h:i:s')
                );
            Yii::app()->db->createCommand()->insert('student_score', $data_db)->execute;
        }
        
        
    }

    public function fetchAllClasses() {
        $sql = "SELECT c.id course_id, c.course_name, c.code, c.is_deleted, b.`name` batch_name, b.id batch_id,
	(select count(1) from students where b.id = batch_id) as student_count, 
(select count(1) from students where gender = 'F' and b.id = batch_id) as female_count,
(select count(1) from students where gender = 'M' and b.id = batch_id) as male_count "
                . "FROM courses c "
                . "inner JOIN batches b on b.course_id = c.id "
                . "WHERE c.is_deleted = 0 AND b.is_deleted = 0 ";


        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function fetch_student_score($params) {
        $sql = "SELECT
	sc.*, s.`name` subject
            FROM
                    student_score sc
            left JOIN subjects s on s.id = sc.sub_id
            WHERE
                    sc.session_id = {$params['session_id']}
            AND sc.term_id = {$params['term_id']}
            and stuid = {$params['stuid']}
            AND sc.cid = {$params['cid']}";

        if ($params['sub_id']) {
            $sql .= " AND sub_id = {$params['sub_id']} ";
        }
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function fetch_student_score1($params) {
        $sql = "select s.first_name, s.last_name, sb.name subject, sb.id sub_id, sc.ca, sc.exam
        from students s
        INNER JOIN subjects sb ON s.batch_id = sb.batch_id
        LEFT JOIN student_score sc ON sb.id = sc.sub_id 
        AND sc.stuid = {$params['stuid']} "
        . "AND sc.session_id ={$params['session_id']} "
        . "AND sc.term_id = {$params['term_id']}
        WHERE s.id= {$params['stuid']}";

        
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
    
    public function getMyPsycho($student_id, $term_id, $session_id){
        $sql = "SELECT * FROM psychomotor WHERE student_id = {$student_id} AND term_id = {$term_id} AND session_id= {$session_id} ";
        return Yii::app()->db->createCommand($sql)->queryRow();
    }
    
    public function addPsychomotor($data){
        return Yii::app()->db->createCommand()->insert('psychomotor', $data);
    }
    
    public function checkDelete($student_id, $term_id, $session_id){
        if ($this->getMyPsycho($student_id, $term_id, $session_id)){
            $sql = "DELETE FROM psychomotor WHERE student_id = {$student_id} AND term_id = {$term_id} AND session_id= {$session_id}";
            return Yii::app()->db->createCommand($sql)->execute();
        }
        return;
    }

    public static function fetch_student_score_only($params) {
        $sql = "SELECT
	sc.*, s.`name` subject, t.term, se.session_name
            FROM
                    student_score sc
            left JOIN subjects s on s.id = sc.sub_id
            LEFT JOIN session se ON se.session_id = sc.session_id
            LEFT JOIN term t ON t.term_id = sc.term_id
            WHERE  
            stuid = {$params['stuid']}";

        if ($params['sub_id']) {
            $sql .= " AND sub_id = {$params['sub_id']} ";
        }
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

}
