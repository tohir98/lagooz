<?php

/**
 * @property Exam_model $e_model Description
 */
class DefaultController extends RController {
    private $e_model;

    public function actionIndex() {
        $this->e_model = new Exam_model();
        $this->render('index', array(
            'courses' => $this->e_model->fetchAllClasses()
        ));
    }
    
    

}
