<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/justgage.1.0.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/raphael.2.1.0.min.js"></script>

<?php
$this->breadcrumbs = array(
    $this->module->id,
);
?>
<link href="css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('left_side'); ?>
        </td>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="75%">
                        <div style="padding-left:20px;">

                            <div class="clear"></div>

                            <div class="pdtab_Con" style="width:97%">
                                <div style="font-size:13px; padding:5px 0px"><strong>Class Rooms</strong></div>
                                <div class="pdtab_Con">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr class="pdtab-h">
                                            <td>
                                                SN
                                            </td>
                                            <td align="center">
                                                Class Name
                                            </td>
                                            <td align="center">
                                                Arm
                                            </td>
                                            <td align="center">
                                                Student Count
                                            </td>
                                            <td align="center">
                                                Female
                                            </td>
                                            <td align="center">
                                                Male
                                            </td>
                                            <td align="center">
                                                Action
                                            </td>
                                        </tr>


                                        <?php
                                        if (!empty($courses)) {
                                            $sn = 0;
                                            foreach ($courses as $course) {
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn; ?></td>
                                                    <td align="center"><?= $course['course_name']; ?></td>
                                                    <td align="center"><?= $course['batch_name']; ?></td>
                                                    <td align="center"><?= (int) $course['student_count'] > 0 ? $course['student_count'] : '-'; ?></td>
                                                    <td align="center"><?= (int) $course['female_count'] > 0 ? $course['female_count'] : '-'; ?></td>
                                                    <td align="center"><?= (int) $course['male_count'] > 0 ? $course['male_count'] : '-'; ?></td>

                                                    <td align="center">
                                                        <a href="index.php?r=examination/exam/view_details&cid=<?= $course['course_id'] ?>&bid=<?= $course['batch_id'] ?>" title="view students in class">view</a>
                                                    </td>
                                                </tr>	
                                                <?php
                                            }
                                        } else {
                                            //echo '<td align="center" colspan="5"><strong>'.'No Recent Exams!'.'</td>';
                                            echo '<td align="center" colspan="5"><strong>' . Yii::t('examination', 'No Classes found') . '</td>';
                                        }
                                        ?>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


