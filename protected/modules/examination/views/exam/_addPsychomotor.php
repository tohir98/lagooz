<form method="post" name="frmPsychomotor">
    <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr class="stripe">
                <td>Fluency</td>
                <td><input required type="number" name="psychomotor[fluency]" max="5" min="0" value="<?= $myPsycho['fluency'] ? : '' ?>" title="pls enter a value"></td>
                <td>Self Control</td>
                <td><input required type="number" name="psychomotor[self_control]" max="5" min="0" value="<?= $myPsycho['self_control'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr>
                <td>Sense of Responsibility</td>
                <td><input required type="number" name="psychomotor[sense_of_responsibility]" max="5" min="0" value="<?= $myPsycho['sense_of_responsibility'] ? : '' ?>" title="pls enter a value"></td>
                <td>Promptness</td>
                <td><input required type="number" name="psychomotor[promptness]" max="5" min="0" value="<?= $myPsycho['promptness'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr class="stripe">
                <td>Organizational Ability</td>
                <td><input required type="number" name="psychomotor[organizational_ability]" max="5" min="0" value="<?= $myPsycho['organizational_ability'] ? : '' ?>" title="pls enter a value"></td>
                <td>Honesty</td>
                <td><input required type="number" name="psychomotor[honesty]" max="5" min="0" value="<?= $myPsycho['honesty'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr>
                <td>Games</td>
                <td><input required type="number" name="psychomotor[games]" max="5" min="0" value="<?= $myPsycho['games'] ? : '' ?>" title="pls enter a value"></td>
                <td>Punctuality</td>
                <td><input required type="number" name="psychomotor[punctuality]" max="5" min="0" value="<?= $myPsycho['punctuality'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr class="stripe">
                <td>Reliability</td>
                <td><input required type="number" name="psychomotor[reliability]" max="5" min="0" value="<?= $myPsycho['reliability'] ? : '' ?>" title="pls enter a value"></td>
                <td>Politeness</td>
                <td><input required type="number" name="psychomotor[politeness]" max="5" min="0" value="<?= $myPsycho['politeness'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr>
                <td>Student Relationship</td>
                <td><input required type="number" name="psychomotor[student_relationship]" max="5" min="0" value="<?= $myPsycho['student_relationship'] ? : '' ?>" title="pls enter a value"></td>
                <td>Spirit of Co-operation</td>
                <td><input required type="number" name="psychomotor[spirit_of_coperation]" max="5" min="0" value="<?= $myPsycho['spirit_of_coperation'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr class="stripe">
                <td>Attentiveness</td>
                <td><input required type="number" name="psychomotor[attentiveness]" max="5" min="0" value="<?= $myPsycho['attentiveness'] ? : '' ?>" title="pls enter a value"></td>
                <td>Initiative</td>
                <td><input required type="number" name="psychomotor[initiative]" max="5" min="0" value="<?= $myPsycho['initiative'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr>
                <td>Staff Relationship</td>
                <td><input required type="number" name="psychomotor[staff_relationship]" max="5" min="0" value="<?= $myPsycho['staff_relationship'] ? : '' ?>" title="pls enter a value"></td>
                <td>Hand Writing</td>
                <td><input required type="number" name="psychomotor[handwriting]" max="5" min="0" value="<?= $myPsycho['handwriting'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr class="stripe">
                <td>Musical Skill</td>
                <td><input required type="number" name="psychomotor[musical_skill]" max="5" min="0" value="<?= $myPsycho['musical_skill'] ? : '' ?>" title="pls enter a value"></td>
                <td>Class Attendance</td>
                <td><input required type="number" name="psychomotor[class_attendance]" max="5" min="0" value="<?= $myPsycho['class_attendance'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr>
                <td>Neatness</td>
                <td><input required type="number" name="psychomotor[neatness]" max="5" min="0" value="<?= $myPsycho['neatness'] ? : '' ?>" title="pls enter a value"></td>
                <td>Perseverance</td>
                <td><input required type="number" name="psychomotor[perseverance]" max="5" min="0" value="<?= $myPsycho['perseverance'] ? : '' ?>" title="pls enter a value"></td>
            </tr>
            <tr class="stripe">
                <td>
                    <input type="hidden" name="psychomotor[student_id]" value="<?= $_GET['student_id'] ?>">
                    <input type="hidden" name="psychomotor[term_id]" value="<?= $term_id ?>">
                    <input type="hidden" name="psychomotor[session_id]" value="<?= $session_id ?>">
                </td>
                <td>&nbsp;</td>
                <td><input type="submit" value="Save" class="formbut"></td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</form>