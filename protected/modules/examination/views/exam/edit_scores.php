<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/justgage.1.0.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/raphael.2.1.0.min.js"></script>

<?php
$this->breadcrumbs = array(
    $this->module->id,
    'view Details' => array('view_details&&cid=' . $_GET['cid'] . '&bid=' . $_GET['bid']),
    'Exam Scores' => array('exam_scores')
);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('/default/left_side'); ?>
        </td>
        <td valign="top">
            <?php $student = Students::model()->findByPk($_GET['student_id']);
            ?>
            <div class="cont_right formWrapper">
                <h1>Examination Scores :<?= $student->first_name; ?>&nbsp;<?= $student->last_name; ?><br></h1>
                <div class="edit_bttns">
                    <ul>
                        <li>
                            <a class="edit last" href="index.php?r=examination/exam/exam_scores&student_id=<?= $_GET['student_id'] ?>&cid=<?= $_GET['cid'] ?>&bid=<?= $_GET['bid'] ?>"><span>Back</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="75%">
                        <div style="padding-left:20px;">

                            <div class="clear"></div>

                            <div class="pdtab_Con" style="width:97%">

                                <div class="pdtab_Con">
                                    <form method="post" action="">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr class="pdtab-h">
                                            <td align="center"> Subject </td>
                                            <td align="center"> CA (30) </td>
                                            <td align="center">Term Exam (70)</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <?php
                                        if (!empty($scores)) :
                                            $sn = 0;
                                            foreach ($scores as $score) :
                                                ?>
                                                <tr>
                                                    <td align="center"><?= $score['subject']; ?></td>
                                                    <td align="center"><input type="number" name="ca" value="<?= $score['ca']; ?>" max="30" maxlength="2" /></td>
                                                    <td align="center">
                                                        <input required type="number" name="exam" value="<?= $score['exam']; ?>" max="70" maxlength="2" />
                                                    </td>
                                                    <td required align="center"><button type="submit" name="update_score"> Update</button></td>
                                                </tr>
                                                <?php
                                            endforeach;
                                        else : 
                                            
//                                            $sb = new Subjects(); 
                                        $student = Subjects::model()->findByPk($_GET['sub_id']);
                                        
                                        ?>
                                            <tr>
                                                    <td align="center"><?= $student->name; ?></td>
                                                    <td align="center"><input type="number" name="ca" max="30" maxlength="2" /></td>
                                                    <td align="center">
                                                        <input required type="number" name="exam" max="70" maxlength="2" />
                                                    </td>
                                                    <td required align="center"><button type="submit" name="update_score"> Add Score</button></td>
                                                </tr>
                                        <?php endif;
                                        ?>

                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



