<style>
    .stripe{
        background-color: #eee;
    }

    .stripe > td{
        padding: 5px;
    }

    input{
        width: 50px;
        line-height: 20px;
    }
</style>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/justgage.1.0.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/raphael.2.1.0.min.js"></script>

<?php
$this->breadcrumbs = array(
    $this->module->id,
    'view Details' => array('view_details&&cid=' . $_GET['cid'] . '&bid=' . $_GET['bid']),
    'Exam Scores' => array('exam_scores')
);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('/default/left_side'); ?>
        </td>
        <td valign="top">
            <?php
            if (Yii::app()->user->hasFlash('success')) {
                ?>
                <div class="infogreen_bx" style="margin:10px 0 10px 10px; width:575px;"><?php echo Yii::app()->user->getFlash('success'); ?></div>
                <?php
            } else if (Yii::app()->user->hasFlash('error')) {
                ?>
                <div class="infored_bx" style="margin:10px 0 10px 10px; width:575px;"><?php echo Yii::app()->user->getFlash('error'); ?></div>
                <?php
            }
            ?>
            <?php $student = Students::model()->findByPk($_GET['student_id']);
            ?>
            <div class="cont_right formWrapper">
                <h1>Examination Scores :<?= ucfirst($student->first_name); ?>&nbsp;<?= ucfirst($student->last_name); ?><br></h1>
                <div class="edit_bttns">
                    <ul>
                        <li>
                            <a class="edit first" href="index.php?r=teachersportal/default/reportSheet&student_id=<?= $_GET['student_id'] ?>&cid=<?= $_GET['bid'] ?>" target="_blank"><span>Report Sheet</span></a>
                        </li>
                        <li>
                            <a class="edit last" href="index.php?r=examination/exam/view_details&cid=<?= $_GET['cid']?>&bid=<?= $_GET['bid']?>"><span>Back</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="75%">
                        <div style="padding-left:20px;">

                            <div class="clear"></div>

                            <div class="pdtab_Con" style="width:97%">

                                <div class="pdtab_Con">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr class="pdtab-h">
                                            <td align="center"> Subject </td>
                                            <td align="center"> CA (30) </td>
                                            <td align="center">Term Exam (70)</td>
                                            <td align="center">Total</td>
                                            <td align="center"> Position</td>
                                            <td align="center"> Average</td>
                                            <td align="center"> Highest</td>
                                            <td align="center"> Lowest</td>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                        <?php
                                        if (!empty($scores)) {
                                            $sn = 0;
                                            foreach ($scores as $score) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?= $score['subject']; ?></td>
                                                    <td align="center"><?= $score['ca'] ? : 0; ?></td>
                                                    <td align="center"><?= $score['exam'] ? : 0; ?></td>
                                                    <td align="center"><?= $score['ca'] + $score['exam']; ?></td>
                                                    <td align="center">-</td>
                                                    <td align="center">-</td>
                                                    <td align="center">-</td>
                                                    <td align="center">-</td>
                                                    <td align="center">
                                                        <a title="Click here to edit score" href="index.php?r=examination/exam/edit_scores&student_id=<?= $_GET['student_id'] ?>&cid=<?= $_GET['cid'] ?>&bid=<?= $_GET['bid'] ?>&sub_id=<?= $score['sub_id'] ?>">edit</a>
                                                    </td>
                                                </tr>	
                                                <?php
                                            }
                                        } else {
                                            //echo '<td align="center" colspan="5"><strong>'.'No Recent Exams!'.'</td>';
                                            echo '<td align="center" colspan="9"><strong>' . Yii::t('examination', 'No scores found') . '</td>';
                                        }
                                        ?>

                                    </table>
                                </div>

                                <h3 style="text-align: center">Psychomotor</h3>
                                <?php include '_addPsychomotor.php'; ?>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



