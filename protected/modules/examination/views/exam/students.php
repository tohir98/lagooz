<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/justgage.1.0.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/raphael.2.1.0.min.js"></script>

<?php
$this->breadcrumbs = array(
    $this->module->id => 'index.php?r=examination',
    'view Details'
);
?>
<link href="css/live.css" rel="stylesheet" type="text/css">
<style type="text/css">
    th{ background:#D2EEF0; padding:8px; border:1px #EFEFEF}
    td{ padding:5px; border:1px #E6E6E6 solid}
    td a{ padding:5px; color:#FF8000; font-weight:bold}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
            <?php $this->renderPartial('/default/left_side');?>
        </td>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="75%">
                        <div style="padding-left:20px;">

                            <div class="clear"></div>

                            <div class="pdtab_Con" style="width:97%">
                                <div style="font-size:13px; padding:5px 0px"><strong>Class Rooms</strong></div>
                                <div class="pdtab_Con">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr class="pdtab-h">
                                            <td align="center"> SN </td>
                                            <td align="center"> Student No </td>
                                            <td align="center">Full Name</td>
                                            <td align="center">Gender</td>
                                            <td align="center"> Dob</td>
                                            <td align="center">
                                                Action
                                            </td>
                                        </tr>
                                        <?php
                                        if (!empty($students)) {
                                            $sn = 0;
                                            foreach ($students as $student) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?= ++$sn; ?></td>
                                                    <td align="center"><?= $student['admission_no']; ?></td>
                                                    <td align="center"><?= ucfirst($student['first_name']) . ' ' . ucfirst($student['middle_name']) . ' ' .  ucfirst($student['last_name']) ; ?></td>
                                                    <td align="center"><?= $student['gender'] ; ?></td>
                                                    <td align="center"><?= date('d-M-Y', strtotime($student['date_of_birth'])); ?></td>

                                                    <td align="center">
                                                        <a href="index.php?r=examination/exam/exam_scores&student_id=<?= $student['id'] ?>&cid=<?= $_GET['cid'] ?>&bid=<?= $_GET['bid'] ?>" title="view students result">view</a>
                                                    </td>
                                                </tr>	
                                                <?php
                                            }
                                        } else {
                                            //echo '<td align="center" colspan="5"><strong>'.'No Recent Exams!'.'</td>';
                                            echo '<td align="center" colspan="5"><strong>' . Yii::t('examination', 'No Classes found') . '</td>';
                                        }
                                        ?>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



