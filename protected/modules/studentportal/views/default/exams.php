<?php
session_start();
require("extends/pubs/virtualphp.php");
if (isset($_POST['print3'])) {
    $stid = $_SESSION['idn2'];
    $term = $_POST['term3'];
    $sess = $_POST['sess'];

    header("Location: " . "extends/sturesult.php?sid=$stid&term=$term&sess=$sess");
}
?>
<div id="parent_Sect">
    <?php $this->renderPartial('leftside'); ?> 
    <?php
    $student = Students::model()->findByAttributes(array('uid' => Yii::app()->user->id));
//    $exam = ExamScores::model()->findAll("student_id=:x", array(':x' => $student->id));
    $exams = Exam_model::fetch_student_score_only(['stuid' => $student->id]);

    $academic_sessions = $academic_terms = $academic_class= [];
    // get unique sessions/terms
    if (!empty($exams)) {
        foreach ($exams as $exam) {
            $academic_sessions[$exam['session_id']] = $exam['session_name'];
            $academic_terms[$exam['term_id']] = $exam['term'];
            $academic_class[$exam['cid']] = $exam['cid'];
        }
    }
    ?>
    <div id="parent_rightSect">
        <div class="parentright_innercon">
            <h1><?php echo Yii::t('studentportal', 'Exams' . $_SESSION['idn2']); ?></h1>
            <div class="profile_top">
                <div class="prof_img">
                    <?php
                    if ($student->photo_data != NULL) {
                        echo '<img  src="' . $this->createUrl('/students/Students/DisplaySavedImage&id=' . $student->primaryKey) . '" alt="' . $student->photo_file_name . '" width="100" height="103" />';
                    } elseif ($student->gender == 'M') {
                        echo '<img  src="images/portal/prof-img_male.png" alt=' . $student->first_name . ' width="100" height="103" />';
                    } elseif ($student->gender == 'F') {
                        echo '<img  src="images/portal/prof-img_female.png" alt=' . $student->first_name . ' width="100" height="103" />';
                    }
                    ?>
                </div>
                <h2><?php echo ucfirst($student->last_name) . ' ' . ucfirst($student->first_name); ?></h2>
                <ul>
                    <li class="rleft"><?php echo Yii::t('studentportal', 'Class :'); ?></li>
                    <li class="rright">
                        <?php
                        $batch = Batches::model()->findByPk($student->batch_id);
                        echo $batch->course123->course_name;
                        ?>
                    </li>
                    <li class="rleft"><?php echo Yii::t('studentportal', 'Class Arm :'); ?></li>
                    <li class="rright"><?php echo $batch->name; ?></li>
                    <li class="rleft"><?php echo Yii::t('studentportal', 'Admission No :'); ?></li>
                    <li class="rright"><?php echo $student->admission_no; ?></li>
                </ul>
            </div> <!-- END div class="profile_top" -->
            <div class="profile_details">
                <h3><?php echo Yii::t('studentportal', 'Personal Report Sheet'); ?></h3>
                <br /> 

                <table width="60%" cellspacing="0" cellpadding="0">
                    <?php
                    if (!empty($academic_sessions)) :
                        foreach ($academic_sessions as $session_id => $session) :
                            foreach ($academic_terms as $term_id => $term) :
                        foreach ($academic_class as $cid => $class) :
                                ?>
                                <tr>
                                    <td><?= $session; ?></td>
                                    <td><?= $term; ?></td>
                                    <td><a href="index.php?r=studentportal/default/ReportSheet&session_id=<?= $session_id?>&term_id=<?=$term_id?>&stuid=<?= $student->id?>&cid=<?=$cid?>">Report Sheet</a></td>
                                </tr>
                            <?php
                            endforeach;
                            endforeach;
                        endforeach;
                    endif;
                    ?>

                </table>
                
            </div>
            <div class="profile_details">
                <h3><?php echo Yii::t('studentportal', 'Assessment'); ?></h3>
                <br />  
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <th>Session</th>
                        <th>Term</th>
                        <th>Subject</th>
                        <th>CA</th>
                        <th>Exam</th>
                        <th>Total</th>
                    </tr>
                    <?php
                    if (empty($exams)) :
                        echo '<tr><td align="center" colspan="6"><i>' . Yii::t('studentportal', 'No Assessments') . '</i></td></tr>';
                    else :
                        foreach ($exams as $exam) :
                            ?>
                            <tr>
                                <td><?= $exam['session_name']; ?></td>
                                <td><?= $exam['term']; ?></td>
                                <td><?= $exam['subject']; ?></td>
                                <td><?= $exam['ca']; ?></td>
                                <td><?= $exam['exam']; ?></td>
                                <td><?= $exam['exam'] + $exam['ca']; ?></td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>    
                </table>
            </div> 


            <!-- END div class="profile_details" -->
        </div> <!-- END div class="parentright_innercon" -->
    </div> <!-- END div id="parent_rightSect" -->
    <div class="clear"></div>
</div> <!-- END div id="parent_Sect" -->
